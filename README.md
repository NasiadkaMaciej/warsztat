# Powiększony nagłówek
Paragraf 1  
Paragraf 2  
Paragraf 3  
~~Przekreślony tekst~~  
**Gruby tekst**  
*Krzywy tekst*  
>Cytat  

1. Lista  
	1. Zagnieżdżona lista numeryczna  
		- Zagnieżdżona lista nienumeryczna  

Żeby skompilować program, wykonaj:
```bash
mkdir -p build
cd build
cmake ..
make
echo "Build completed"
```
Wpisz: ```ls``` w konsolę, żeby wyświetlić pliki i foldery


![Kitten](https://gitlab.com/NasiadkaMaciej/warsztat/-/raw/main/cat.jpeg?ref_type=heads&inline=false)